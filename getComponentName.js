const window = require('vscode').window;

async function getComponentName() {
	const result = await window.showInputBox({
		placeHolder: "write name's component",
        title: "Component Name",
		validateInput: function(inputValue) {
            if(!inputValue){
                return "Cannot be empty";
            }
            if(inputValue.indexOf(' ') >= 0){
                return "No spaces allowed";
            }
            if(inputValue.match(/^\d/)){
                return "Cannot start with number";
            }
            return null
		}
	});

    return result;
}
module.exports = getComponentName