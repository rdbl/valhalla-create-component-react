// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
const vscode =  require('vscode');
const path =  require( 'path');
const fs = require('fs');
const replace = require('replace-in-file');
const getComponentName =  require( './getComponentName');

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed

/**
 * @param {vscode.ExtensionContext} context
 */
function activate(context) {

	// Use the console to output diagnostic information (console.log) and errors (console.error)
	// This line of code will only be executed once when your extension is activated
	console.log('Congratulations, your extension "valhalla-create-component-react" is now active!');

	// The command has been defined in the package.json file
	// Now provide the implementation of the command with  registerCommand
	// The commandId parameter must match the command field in package.json


	let disposable = vscode.commands.registerCommand('valhalla-create-component-react.createBasicReactComponent', async function(uri){

		const currentDir = uri.fsPath;
		const rootExtensionPath = vscode.Uri.file(path.resolve(
			path.join(__dirname, './templates/BasicTemplate')
		)).fsPath
		const componentName = await getComponentName();
		const directory = path.join(currentDir, componentName);

		// TODO : check if folder already exists

		await vscode.workspace.fs.copy(vscode.Uri.file(rootExtensionPath),vscode.Uri.file(directory),{overwrite:false})

		const renameFile = (componentPath,componentName,ext) => {
			const templateFile = path.join(componentPath, componentName, 'XXXXX'+"."+ext);
			const newFile = path.join(componentPath, componentName, componentName +"."+ext)
			fs.renameSync( templateFile , newFile	)
			return newFile
		}

		const newFilePathJs = renameFile(currentDir,componentName,'js')
		renameFile(currentDir,componentName,'scss')
			
		await replace({
			files: newFilePathJs,
			from: /XXXXX/g,
			to: componentName,
		});

		await replace({
			files: path.join(currentDir, componentName, 'index.js'),
			from: /XXXXX/g,
			to: componentName,
		});

		// Display a message box to the user
		vscode.window.showInformationMessage('Hello World from Create React Component! ');
		
	});

	context.subscriptions.push(disposable);
}

// this method is called when your extension is deactivated
function deactivate() {}

module.exports = {
	activate,
	deactivate
}
